# Example ecosystem.json file for PM2
You can read more [here](http://pm2.keymetrics.io/docs/usage/application-declaration/)
```json
{
  "apps": [
    {
      "name": "api:dev",
      
      "env": {
       "NODE_ENV": "production"
      },

      "env_api:dev": {
        "NODE_ENV": "development"
      },

      "script": "./current/entry.js",
      "error_file": "./logs/api.err.log",
      "out_file": "./logs/app.out.log",
      "exec_mode": "fork_mode"
    }
  ]
}
```