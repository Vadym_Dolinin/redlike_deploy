# Shipit

# Installation

```bash
npm install
```

If you want to run 
```bash
shipit env task
```
you have to install
```bash
npm install shipit-cli -g
```
in other case you have to run 
```bach
./node_module/.bin/shipit env task
```

> **Please, verify local and remote [dependencies](https://github.com/shipitjs/shipit-deploy#dependencies)**

# Usage
Every project must have an PM2 process file aka ecosystem file. Example configuration you can see [here](./ecosystem.json.md)

Deploy
```bash
shipit api:dev deploy
```

Rollback
```bash
shipit api:dev rollback
```

We also can run any of registered tasks on remote machine

```bach
shipit api:dev webpack:build
```
