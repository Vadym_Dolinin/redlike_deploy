'use strict';

const chalk = require('chalk');
const clear = require('clear');
const utils = require('shipit-utils');

module.exports = (shipitOrGrunt) => {
  const shipit = utils.getShipit(shipitOrGrunt);

  require('shipit-deploy')(shipit);
  require('./task/npm')(shipit);
  require('./task/pm2')(shipit);
  require('./task/webpack')(shipit);

  clear();
  shipit.log(chalk.bgGreen('Shipit ready'));

  shipit.initConfig({
    default: {
      // Local temporary folder
      workspace: '/tmp/deploy',
      // Remote folder
      deployTo: '/home/temp',
      ignores: ['.git', 'node_modules'],
      keepReleases: 5,
      deleteOnRollback: false,
      shallowClone: true,
      // key: '~/.ssh/id_rsa.pub'
    },

    'api:prod': {
      // key: '',
      workspace: '/tmp/deploy/api_prod',
      servers: 'alexander@127.0.0.1',
      // branch: '',
      // repositoryUrl: ''
    }
  });

  // Events hook
  // @see https://github.com/shipitjs/shipit-deploy#workflow-tasks
  shipit.on('updated', () => shipit.start(['npm:install']));
  shipit.on('npm:installed', () => shipit.start(['webpack:build']));

  // http://pm2.keymetrics.io/docs/tutorials/capistrano-like-deployments#shipit-pm2-example
  shipit.on('cleaned', () => shipit.start(['pm2']));
};