const utils = require('shipit-utils');

module.exports = (shipitOrGrunt) => {
  const shipit = utils.getShipit(shipitOrGrunt);
  require('./task/install')(shipit);
};