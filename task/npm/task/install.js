const utils = require('shipit-utils');
const chalk = require('chalk');
const path = require('path');

module.exports = (shipitOrGrunt) => {
  const shipit = utils.getShipit(shipitOrGrunt);

  const task = () => {
    const releasePath = shipit.releasePath || path.join(shipit.config.deployTo, 'current');

    shipit.log(chalk.blue(`Installing dependencies on ${releasePath}`));

    const cmd = `cd ${releasePath} && npm install`;

    return shipit.remote(cmd).then(() => {
      shipit.log(chalk.green('Dependencies installing success'));
      shipit.emit('npm:installed');
    }).catch((e) => {
      shipit.log(chalk.red.bgWhite(`Dependencies installing failed`));
      throw e;
    });
  };

  utils.registerTask(shipit, 'npm:install', task);
};