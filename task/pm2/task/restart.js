const utils = require('shipit-utils');
const chalk = require('chalk');
const path = require('path');

module.exports = (shipitOrGrunt) => {
  const shipit = utils.getShipit(shipitOrGrunt);

  const task = () => {
    shipit.log(chalk.blue(`Launching pm2`));

    const schema = path.join(
      'current', 
      shipit.config.pm2 && shipit.config.pm2.script ? shipit.config.pm2.script : 'ecosystem.json'
    );

    const cmd = `SOME_APP_ENV="${shipit.environment}" cd ${shipit.config.deployTo} && pm2 startOrRestart --env ${shipit.environment} ${schema}`;

    return shipit.remote(cmd).then(() => {
      shipit.log(chalk.green(`pm2 started`));
      shipit.emit('pm2:started');
    }).catch((e) => {
      shipit.log(chalk.red.bgWhite(`Installing failed`));
      throw e; 
    });
  };

  utils.registerTask(shipit, 'pm2', task);
};