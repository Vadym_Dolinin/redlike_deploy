const utils = require('shipit-utils');
const chalk = require('chalk');
const path = require('path');

module.exports = (shipitOrGrunt) => {
  const shipit = utils.getShipit(shipitOrGrunt);

  const task = () => {
    const releasePath = shipit.releasePath || path.join(shipit.config.deployTo, 'current');
  
    shipit.log(chalk.blue(`Build assets on ${releasePath}`));

    const cmd = `cd ${releasePath} npm run build`;

    return shipit.remote(cmd).then(() => {
      shipit.log(chalk.green(`Webpack build success`));
      shipit.emit(`webpack:built`);
    }).catch((e) => {
      shipit.log(chalk.red.bgWhite(`Webpack build failed`));
      throw e;
    });
  };

  utils.registerTask(shipit, 'webpack:build', task);
};